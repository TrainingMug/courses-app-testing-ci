import { Routes } from '@angular/router';
import { Component } from '@angular/core';

@Component({
    template: 'Login Dummy Component Template'
})
export class LoginDummyComponent{}


@Component({
    template: 'Courses List Dummy Component Template'
})
export class CoursesListDummyComponent{}


 export   const routes: Routes = [{
        path:'login', component:LoginDummyComponent
      },{
        path:'courses-list',component:CoursesListDummyComponent
      },
      {
        path:'',redirectTo:'/courses-list',pathMatch:'full'
      }];

