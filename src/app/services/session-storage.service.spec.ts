import { TestBed } from '@angular/core/testing';

import { SessionStorageService } from './session-storage.service';

describe('SessionStorageService', () => {

  let sessionStorageService : SessionStorageService;

  let store = {};
  const mockSessionStorage = {
    getItem : (key : string) :string =>{
      return key in store ? store[key] : null;
    },
    setItem : (key : string, value : string) =>{
      store[key] = `${value}`;
    },
    removeItem: (key : string) =>{
      delete store[key];
    },
    clear: () =>{
      store = {};
    }

  }


  beforeEach(() => {  
    TestBed.configureTestingModule({})
    sessionStorageService = TestBed.get(SessionStorageService);
  });



  it('should be created', () => {
    expect(sessionStorageService).toBeTruthy();
  });

  it('#storeUserSessionInfo should store the user session info',()=>{
    spyOn(sessionStorage, 'getItem')
      .and.callFake(mockSessionStorage.getItem);
    spyOn(sessionStorage, 'setItem')
      .and.callFake(mockSessionStorage.setItem);
    spyOn(sessionStorage, 'removeItem')
      .and.callFake(mockSessionStorage.removeItem);
    spyOn(sessionStorage, 'clear')
      .and.callFake(mockSessionStorage.clear);
      sessionStorageService.storeUserSessionInfo('1111','testuser111');
    expect(sessionStorageService.getUserSessionInfo()).toEqual({userId:'1111', username:'testuser111',role:'user'});
  });

  it('#clearUserSessionInfo should clear the user session info',()=>{
     
    spyOn(sessionStorage, 'getItem')
    .and.callFake(mockSessionStorage.getItem);  
  spyOn(sessionStorage, 'clear')
    .and.callFake(mockSessionStorage.clear);

    sessionStorageService.clearUserSessionInfo();
    expect(sessionStorageService.getUserSessionInfo()).toEqual({userId:null,username:null,role:null});

  });

});