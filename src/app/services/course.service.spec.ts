import { TestBed } from '@angular/core/testing';
import { HttpTestingController, HttpClientTestingModule} from '@angular/common/http/testing';

import { CourseService } from './course.service';
import { HttpErrorResponse } from '@angular/common/http';


describe('CourseService', () => {

  const testConfig = {

    getCourses : {
      positive : [{
        "id": 1,
        "name": "Introduction to HTML5 Test",
        "description": "This course teaches you the basic fundamentals of web page development using HTML5",
        "imageURL": "../../assets/images/html5_1.jpg",
        "mentor": "Madhu Samala"
      },{
        "id": 2,
        "name": "Web Page Development using HTML5 Test",
        "description": "This course teaches you to develop awesome web pages using HTML5",
        "imageURL": "../../assets/images/html5_2.png",
        "mentor": "Madhu Samala"
      }],
      empty: []
    },
      COURSES_BACKEND_URL:'http://localhost:3000/courses',
      error404 : {
      message: 'Http failure response for http://localhost:3000/courses: 404 Not Found',
      name: 'HttpErrorResponse',
      ok: false,
      status : 404,
      statusText: 'Not Found',
      url: 'http://localhost:3000/courses'
      }
    
  
  }
  

  let httpMock : HttpTestingController;
  let courseService : CourseService;
  let mockResponsePositive : any;
  let mockResponseNegative : any;
  let mockResponseError404 : any;
  const backendURL : string = testConfig.COURSES_BACKEND_URL;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports:[
        HttpClientTestingModule
      ],
      providers:[CourseService]
    })
    
  });

  beforeEach(() =>{
    httpMock = TestBed.get(HttpTestingController);
    courseService = TestBed.get(CourseService);
  })
  afterEach(() =>{
    //After every http request, 
    //we need to assert that there are no more pending requests
   httpMock.verify();
 })




  it('should be created', () => {
    expect(courseService).toBeTruthy();
  });


  it('#getNotes should return the courses list as response ' ,()=>{
    mockResponsePositive = testConfig.getCourses.positive;
    courseService.getCourses().subscribe( res =>{    //  expect(res).toBeNull();       
      expect(res).toBeDefined('getCourses is returning empty courses');
      expect(res).toEqual(mockResponsePositive,'');
    })
    const mockRequest = httpMock.expectOne(backendURL);
    mockRequest.flush(mockResponsePositive);
    expect(mockRequest.request.url).toEqual(backendURL,`Request URL must be 'http://localhost:3000/courses'`);
    expect(mockRequest.request.method).toBe('GET','Http Request type must be GET');
    
  });

  it('#getNotes should handle if the courses list is empty' , ()=>{
    mockResponseNegative = testConfig.getCourses.empty;
    courseService.getCourses().subscribe( res =>{   
         //expect(res).toBeNull();       
      expect(res).toBeDefined('getCourses is not returning the response');
      expect(res.length).toBe(0,'getCourses is not returning empty courses')
    })
    const mockRequest = httpMock.expectOne(backendURL);
    mockRequest.flush(mockResponseNegative);
    
  });
  it('#getCourses should handle the error for invalid URL' ,()=>{
    mockResponseError404 = testConfig.error404;
  
    courseService.getCourses().subscribe(res =>{},
                                        (error : HttpErrorResponse) =>{
                                          console.log('error->', error);
                                      expect(error).toBeDefined();
                                      expect(error.error.status).toBe(404,'Should handle invalid URL')
                                       
                                        })
  
    const mockRequest = httpMock.expectOne(backendURL);
    
    mockRequest.error(mockResponseError404);
  
  });
  // #getCourses should handle the network failures , connection timeouts, offline etc.
it('#getCourses should handle the network failure, connection timeouts', ()=>{

  courseService.getCourses().subscribe(res =>{},
    (error : HttpErrorResponse) =>{
      console.log('error->', error);
  expect(error).toBeDefined();
  expect(error.error.message).toBe('Network Failure','Should handle Network failures')
   
    });

  const mockRequest = httpMock.expectOne(backendURL);
  const mockError = new ErrorEvent('Network Error',{
    message: 'Network Failure',
    
  });
  mockRequest.error(mockError);

})
/*
it('#addCourse should return the added course as reponse',() =>{

  courseService.addCourse(mockCourseToBeAdded : Course).subscribe(addedCourse =>{
    expect(addedCourse).toBe(mockCourseToBeAdded);
  });
  const mockRequest = httpMock.expectOne(backendURL);
  expect(mockRequest.request.method).toBe('POST');
  expect(mockRequest.request.body).toEqual(mockCourseToBeAdded);

  //mockResponse
  const expectedResponse = new HttpResponse(
    { status : 200, statusText : 'OK', body: mockCourseToBeAdded};

  );
  mockRequest.event(expectedResponse);
*/


  
  




});