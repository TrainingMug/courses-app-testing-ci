import { TestBed, async, ComponentFixture } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { AppComponent } from './app.component';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { By } from '@angular/platform-browser';


describe('AppComponent', () => {

  let appComponent : AppComponent;
 let fixture : ComponentFixture<AppComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule
      ],
      declarations: [
        AppComponent
      ],
      schemas:[NO_ERRORS_SCHEMA]
    }).compileComponents();
  }));

  beforeEach(()=>{
    fixture = TestBed.createComponent(AppComponent);
    appComponent = fixture.debugElement.componentInstance;
 })



  it('should create the app', () => {
    
    expect(appComponent).toBeTruthy();
  });


  it('AppComponent should contain headercomponent',()=>{
    let headerDebugEle = fixture.debugElement.query(By.css('app-header'));
    expect(headerDebugEle.nativeElement).toBeDefined();
  });
  it('AppComponent should contain routeroutlet',()=>{
    let routerOutletDebugEle = fixture.debugElement.query(By.css('router-outlet'));
    expect(routerOutletDebugEle.nativeElement).toBeDefined();
  })

  it('AppComponent should contain footercomponent',()=>{
    let footerDebugEle = fixture.debugElement.query(By.css('app-footer'));
    expect(footerDebugEle.nativeElement).toBeDefined();
  })



  
});
