import { LoginDummyComponent, CoursesListDummyComponent } from './../../router.test';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { async, ComponentFixture, TestBed, tick, fakeAsync } from '@angular/core/testing';

import { HeaderComponent } from './header.component';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatButtonModule } from '@angular/material/button';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatIconModule } from '@angular/material/icon';
import { MatListModule } from '@angular/material/list';
import { MatSelectModule } from '@angular/material/select';
import { MatCardModule } from '@angular/material/card';
import {MatInputModule} from '@angular/material/input';
import {MatSnackBarModule} from '@angular/material/snack-bar';
import {MatExpansionModule} from '@angular/material/expansion';
import { RouterTestingModule } from '@angular/router/testing';
import { By } from '@angular/platform-browser';
import { Router } from '@angular/router';
import { Location } from '@angular/common';
import { routes } from 'src/app/router.test';
import { SessionStorageService } from 'src/app/services/session-storage.service';

describe('HeaderComponent', () => {
  let component: HeaderComponent;
  let fixture: ComponentFixture<HeaderComponent>;
  let location : Location;
  let sessionStorageService : SessionStorageService;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports:[ 
        MatToolbarModule,
        MatButtonModule,
        MatSidenavModule,
        MatIconModule,
        MatListModule,
        MatSelectModule,
        MatCardModule,
        MatInputModule,
        MatSnackBarModule,
        MatExpansionModule,
        RouterTestingModule,
        BrowserAnimationsModule,
        RouterTestingModule.withRoutes(routes)],
      declarations: [ HeaderComponent , LoginDummyComponent,CoursesListDummyComponent]
      
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HeaderComponent);
    component = fixture.componentInstance;
    location = TestBed.get(Location);
    sessionStorageService = fixture.debugElement.injector.get(SessionStorageService);
    fixture.detectChanges();
  });

  it('should create HeaderComponent', () => {
    expect(component).toBeTruthy();
  });

  it('isUserLoggedIn should be false on the instantiation' ,()=>{
    expect(component.isUserLoggedIn).toBeFalsy();
  })
  it('should display login button when user not loggedin ',()=>{
    let loginButtonDebugEle = fixture.debugElement.query(By.css('.login-button'));
    expect(loginButtonDebugEle.nativeElement).toBeDefined();
  });

  it('should display logout button when the user is loggedin ',()=>{
    component.isUserLoggedIn = true;
    fixture.detectChanges();
    let logoutButtonDebugEle = fixture.debugElement.query(By.css('.logout-button'));
    expect(logoutButtonDebugEle.nativeElement).toBeDefined();

  });

  it('should display navlinks when the user is loggedIn' ,()=>{
    component.isUserLoggedIn = true;
    fixture.detectChanges();
    let navLinksEle = fixture.debugElement.query(By.css('mat-nav-list'));
    expect(navLinksEle.nativeElement).toBeDefined();
  });

  it('should have the proper routes paths in navlinks',()=>{
    component.isUserLoggedIn = true;
    fixture.detectChanges();
    let navListItemsDebugEle = fixture.debugElement.queryAll(By.css('a'));
    expect(navListItemsDebugEle[0].nativeElement.getAttribute('routerLink')).toBe('/courses-list');
  
  })

  it('should route to login on click of login button' ,fakeAsync(()=>{
    component.isUserLoggedIn = false;
    fixture.detectChanges();
    let loginButtonDebugEle = fixture.debugElement.query(By.css('.login-button'));
    if(loginButtonDebugEle){
      let loginButtonNativeEle = loginButtonDebugEle.nativeElement;
      loginButtonNativeEle.click();
      tick();
      expect(location.path()).toContain('/login');
    }
    else{
      expect(false).toBe(true,'should have an login button element with class .login-button');
    }

  }));

  it('should navigate to courses list when the user logs in and clicks on courseslist navlink' , fakeAsync(()=>{
    component.isUserLoggedIn = true;
    fixture.detectChanges();
    let navListItemsDebugEle = fixture.debugElement.queryAll(By.css('a'));
    //expect(navListItemsDebugEle[0].nativeElement.getAttribute('routerLink')).toBe('/courses-list');
    navListItemsDebugEle[0].nativeElement.click();
    tick();
    expect(location.path()).toContain('/courses-list');

  }));

  it('should route to login when the user clicks on logout button', fakeAsync(()=>{
    component.isUserLoggedIn = true;
    fixture.detectChanges();
    let logoutButtonDebugEle = fixture.debugElement.query(By.css('.logout-button'));
    logoutButtonDebugEle.nativeElement.click();
    tick();
    expect(location.path()).toContain('/login');
  }));

  it('should set the isUserLoggedIn to false when the user click on logout button',fakeAsync(()=>{
    component.isUserLoggedIn = true;
    fixture.detectChanges();
    let logoutButtonDebugEle = fixture.debugElement.query(By.css('.logout-button'));
    logoutButtonDebugEle.nativeElement.click();
    tick();
    fixture.detectChanges();
    expect(component.isUserLoggedIn).toBeFalsy();
  }));

  it('should clear the sessionstorage information when the user clicks on logout button',fakeAsync(()=>{
    spyOn(sessionStorageService,'getUserSessionInfo').and.returnValue({userId:null,username:null,role:null});
    component.isUserLoggedIn = true;
    fixture.detectChanges();
    let logoutButtonDebugEle = fixture.debugElement.query(By.css('.logout-button'));
    logoutButtonDebugEle.nativeElement.click();
    tick();
    fixture.detectChanges();
    expect(sessionStorageService.getUserSessionInfo()).toEqual({userId:null,username:null,role:null});
  }))
});
