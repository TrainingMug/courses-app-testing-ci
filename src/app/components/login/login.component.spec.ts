import { SessionStorageService } from './../../services/session-storage.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { LoginDummyComponent, CoursesListDummyComponent } from './../../router.test';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { async, ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatButtonModule } from '@angular/material/button';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatIconModule } from '@angular/material/icon';
import { MatListModule } from '@angular/material/list';
import { MatSelectModule } from '@angular/material/select';
import { MatCardModule } from '@angular/material/card';
import {MatInputModule} from '@angular/material/input';
import {MatSnackBarModule} from '@angular/material/snack-bar';
import {MatExpansionModule} from '@angular/material/expansion';
import { RouterTestingModule } from '@angular/router/testing';

import { LoginComponent } from './login.component';
import { routes } from 'src/app/router.test';
import { FormsModule, ReactiveFormsModule, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { UserService } from 'src/app/services/user.service';
import { LoginService } from 'src/app/services/login.service';
import { RouterService } from 'src/app/services/router.service';
import { By } from '@angular/platform-browser';
import { User } from 'src/app/models/user';
import { Location } from '@angular/common';

fdescribe('LoginComponent', () => {
  let component: LoginComponent;
  let fixture: ComponentFixture<LoginComponent>;
  let userService : UserService;
  let sessionStorageService: SessionStorageService;
  let loginService : LoginService;
  let routerService : RouterService;
  let loginForm : FormGroup;
  let location : Location;

  const formBuilder : FormBuilder = new FormBuilder();
  let spyLoginUser : any;

  let mockUserPromise : Promise<User> = new Promise((resolve,reject) =>{
             let user = new User();
             user.id = 111,
             user.username = 'testuser111',
             user.email = 'testuser111@gmail.com',
             user.password = 'testuser111',
             user.favouriteCoursesList = [];
             resolve(user);
  });
  let mockUserNotFoundPromise : Promise<User> = new Promise((resolve,reject)=>{
       resolve(undefined);
  });
  let serverErrorPromise : Promise<any> = new Promise((resolve,reject) =>{
     reject(new Error("Some Internal Error Occurred.."));
  });

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports:[MatToolbarModule,
        MatButtonModule,
        MatSidenavModule,
        MatIconModule,
        MatListModule,
        MatSelectModule,
        MatCardModule,
        MatInputModule,
        MatSnackBarModule,
        MatExpansionModule,
        RouterTestingModule,
        BrowserAnimationsModule,
        FormsModule,
        ReactiveFormsModule,  
        HttpClientTestingModule,      
        RouterTestingModule.withRoutes(routes)],
      declarations: [ LoginComponent , LoginDummyComponent,CoursesListDummyComponent  ],
      providers:[{provide: FormBuilder, useValue: formBuilder}]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoginComponent);
    component = fixture.componentInstance;
    loginService = fixture.debugElement.injector.get(LoginService);
    sessionStorageService = fixture.debugElement.injector.get(SessionStorageService);
    routerService = fixture.debugElement.injector.get(RouterService);
    userService = fixture.debugElement.injector.get(UserService);
    location = TestBed.get(Location);
    //Creating a form
    component.loginForm = formBuilder.group({
      username : ['',Validators.compose([Validators.required,Validators.minLength(5)])],
      password : ['',Validators.compose([Validators.required,Validators.minLength(6)])]
    })
    

    fixture.detectChanges();
    

  });

  it('should create LoginComponent', () => {
    expect(component).toBeTruthy();
  });

  it('should set the form status invalid if the username or password is empty',()=>{
    component.loginForm.controls['username'].setValue('');
    component.loginForm.controls['password'].setValue('');
    expect(component.loginForm.valid).toBeFalsy();
    fixture.detectChanges();
    expect(component.loginForm.get('username').hasError('required')).toBeTruthy();
   // component.loginForm.controls['username'] = true;
    // let userNameValidationErrorDebugEle =  fixture.debugElement.query(By.css('.username-validation-error'));
    // expect(userNameValidationErrorDebugEle.nativeElement.textContent).toContain('Username is required');
  })

  it('#validateLogin should login the user into the system',fakeAsync(()=>{
    console.log('#validateLogin should login the user into the system', userService);
    spyOn(sessionStorageService,'getUserSessionInfo').and.returnValue({userId:'111',username:'testuser111',role:'user'});
    spyOn(userService,'validateUser').and.returnValue(mockUserPromise);
    component.loginForm.setValue({username:'testuser111', password:'testuser111'});
    component.validateLogin(component.loginForm.value);
    
    fixture.detectChanges();
    tick(1500);
    expect(sessionStorageService.getUserSessionInfo()).toEqual({userId:'111',username:'testuser111',role:'user'})
    expect(location.path()).toContain('/courses-list');
  }));

  it('#validateLogin should display the proper error message if the user is not found', fakeAsync(()=>{
    spyOn(userService,'validateUser').and.returnValue(mockUserNotFoundPromise);
    component.loginForm.setValue({username:'testuser111', password:'testuser111'});
    component.validateLogin(component.loginForm.value);
    
    fixture.detectChanges();
    tick(1500);
    expect(component.serverErrorMessage).toEqual('No User Exists With this details..');
  }));

  it('#clearErrorMessage should remove the error message after calling ',()=>{
    component.clearErrorMessage();
    expect(component.serverErrorMessage).toEqual('');
  });

});
